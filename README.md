# Aulas do módulo Aplicações  do curso de Ionic#

Para acessar o código das aulas, faça checkout em cada branch nomeado de acordo com cada aula.

Exemplo:
```
#!shell

$ git checkout aula-11
```

### Como criar o ambiente? ###

```
#!shell

$ git clone git@bitbucket.org:raphaelpor/lithium-curso-de-ionic-aplica-es.git
$ cd lithium-curso-de-ionic-aplica-es
$ npm install
$ bower install
```

Curso de Ionic: [http://lithium.net.br](http://lithium.net.br).